'''
    a basic twitter API listener that spills out tweets on screen.
    you can use Ctrl+c to stop the streaming
    
'''

import config #twitter api credential
import json
import tweepy #needs installation

class CustomStreamListener(tweepy.StreamListener):
    def on_data(self, status):
        # on_data supplies status data in the form of seralized json string
        # now, deseralize it use json.loads and save it in tweet
        try:
            tweet = json.loads(status)
            # print the tweet on screen.
            print "%s, %s\n%s\n\n"%(tweet['created_at'],tweet['user']['name'],tweet['text'])
        except:
            pass #exception will be ignored.
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True  # continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  # continue to listen

if __name__ == '__main__':

    #create a auth object
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.oauth_token, config.oauth_token_secret)
    
    #create a listener object
    listener = CustomStreamListener()
        
    #attach the listener object to twitter stream
    stream = tweepy.streaming.Stream(auth, listener)

    # below are different ways of getting public streams
    # see https://dev.twitter.com/streaming/public for more details
    
    #receive a sample of all tweets - that's a lot!
    stream.sample()

    #or, you may choose to track certain topics
    #this could be slow moving
    #stream.filter(track=['bigdata',"big data","data science"])      

    #or, follow certain users
    #but you may have to wait for hours to get one.
    #stream.filter(follow=['3187077']) 
       
    #once the stream started, it will be on a different process
    #so we can continue on to the next block of code
    #this while loop is designed as a monitor, so that it will check 
    #on the status of the listener once in a while.
    #it can also interrupt the listener on keyboard interruption (ctrl+c)    
    while True:
        if stream.running is False:
            print 'Stream stopped!'
            break
        time.sleep(1) #sleep for 1 sec before checking again.
            
    stream.disconnect()
    print 'Bye!'