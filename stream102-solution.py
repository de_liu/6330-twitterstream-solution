'''
    in this example, we introduce some debugging facility
    including _debug_ and _online_ global variables. 
    In _offline_ mode, the listener to take a locally stored tweet. 
    In _debug_ mode, we save problematic tweets and exit
'''

import config #put config.py in the same directory
import json
import sys  
import tweepy #needs installation
from pylib import utils


_debug_ = False #If true, on error, save to local file and stop. otherwise, ignore errors
_online_ = True # If true, read from online sources. Otherwise, from a local file

'''
suggested debugging sequence:
[1] (offline debugging) set _debug_ to True, _online_ to False: 
    Test on a local tweet first to see if there is any error
[2] (online debugging) set both to True: 
    so that you can test there are errors in processing incoming data 
    if there are errors, you save the error tweet to one of the local files.
    after you encounter the error
[3] (production) Set _debug_ to False, _online_ to True: 
    all remaining errors will be ignored
'''

class CustomStreamListener(tweepy.StreamListener):
    def on_data(self, status):
        #on_data provides data in the raw json string
        try:        
            tweet = json.loads(status)
            if tweet.get('delete'):
                return True ## skip delete tweet
            # if tweet.get('lang') != 'en':
                # return False            
        
            print "%s, %s\n%s\n\n"%(tweet['created_at'],tweet['user']['name'],tweet['text'])
        
        except Exception, e:
            # some raw messages are not tweets, e.g. status deletion notices
            # they will cause errors when we access keys that do not exist.
            # see https://dev.twitter.com/streaming/overview/messages-types
            if _debug_:
                #write the tweet to local disk for debugging
                utils.dump_str(status,'debug/tweet.txt')
                raise #stop the listener
            else:
                pass #ignore parsing errors

    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True     #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listen

if __name__ == '__main__':

    #create a listener object
    listener = CustomStreamListener()  

    utils.enable_utf_print() #so that unicode characters can be printed on screen.
    utils.handle_break() # so that when control c is pressed, sys will exit

    if _online_:    
        #create an auth object
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.oauth_token, config.oauth_token_secret)
            
        #attach the listener object to twitter stream,
        stream = tweepy.streaming.Stream(auth, listener)
        
        #you may choose to track certain topics
        #stream.filter(track=['bigdata',"big data","data science"])  
        
        #or receive a sample of all tweets - that's a lot!
        stream.sample()

        while True:
            if stream.running is False:
                print 'Stream stopped!'
                break
            time.sleep(1) #sleep for 1 sec
                
        stream.disconnect()
        print 'Bye!'
    else:
        #offline mode
        print "-- offline mode --"
        # debug/tweet_jp.txt : a tweet contains international (non-ascii) characters
        # debug/tweet_normal.txt: a tweet without anything special.
        # debug/tweet_delete.txt: a delete annoucement
        # debug/tweet_example_entities.txt: a tweet with entities (hashtags etc)

        #listener.on_data(utils.read_file('debug/tweet_jp.txt'))
        listener.on_data(utils.read_file('debug/tweet_delete.txt'))
        #listener.on_data(utils.read_file('debug/tweet_example_entities.txt'))